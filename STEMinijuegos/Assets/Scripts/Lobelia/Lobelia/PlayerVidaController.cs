using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVidaController : MonoBehaviour
{
    private int numVidas = 3;
    public bool invenciblePorDano = false;
    private Rigidbody2D _rigibody;
    private SpriteRenderer _renderer;
    //private float fuerzaDano = 0.8f;
    public RectTransform corazonesVida;
    private float tamanoCorazon = 16f; //Cada coraz�n son 16px
    private AudioSource _audio;
    public AudioClip clipDano;
    private GameObject gameController;

    // Start is called before the first frame update
    void Awake()
    {
        _rigibody = this.GetComponent<Rigidbody2D>();
        _renderer = this.GetComponent<SpriteRenderer>();
        _audio = this.GetComponent<AudioSource>();
        corazonesVida.sizeDelta = new Vector2(tamanoCorazon * numVidas, tamanoCorazon);
        gameController = GameObject.FindGameObjectWithTag("GameControllerLobelia");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Funci�n que se llama al recibir da�o el player
    public void PerderVidaPlayer(int vida)
    {
        //Si no est� en modo invencible, el personaje va a sufrir da�o
        if (!invenciblePorDano)
        {            
            _audio.PlayOneShot(clipDano);
            //Personaje parpadea
            StartCoroutine("DanoVisual");

            numVidas -= vida;
            //El tama�o de la caja de corazones es cada tama�o del coraz�n * vidas actuales
            corazonesVida.sizeDelta = new Vector2(tamanoCorazon * numVidas, tamanoCorazon);

            //Game Over
            if (numVidas <= 0)
            {
                numVidas = 0;
                PlayerMuere();
            }

        }
    }

    //Corrutina que hace que el personaje parpadee cuando reciba da�o
    private IEnumerator DanoVisual()
    {
        invenciblePorDano = true;

       // _rigibody.AddForce(Vector2.up * fuerzaDano, ForceMode2D.Impulse);
        Physics2D.IgnoreLayerCollision(10, 12, true);
        for (int i = 0; i < 5; i++)
        {
            _renderer.enabled = false;
            yield return new WaitForSeconds(0.1f);
            _renderer.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }

        invenciblePorDano = false;        
    }

    //Funci�n que se llama al morir el personaje
    private void PlayerMuere()
    {
        gameController.GetComponent<GameLobeliaController>().StartCoroutine("FinPartida");
    }
}
