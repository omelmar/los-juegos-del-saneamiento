using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAtaqueParaguasController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    //Funci�n que controla si el enemigo entra en el collider de ataque con el paraguas
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.gameObject.GetComponent<EnemigoBasicoVidaController>() != null) && (collision.gameObject.tag == "EnemigoDano"))
        {
            collision.gameObject.GetComponent<EnemigoBasicoVidaController>().PerderVida();
            //collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right, ForceMode2D.Impulse);
        }
        else if ((collision.gameObject.GetComponent<EnemigoCascoVidaController>() != null) && (collision.gameObject.tag == "EnemigoCasco"))
            collision.gameObject.GetComponent<EnemigoCascoVidaController>().PerderVida();
        else if ((collision.gameObject.GetComponent<EnemigoEspadaVidaController>() != null) && (collision.gameObject.tag == "EnemigoEspada"))
            if (collision.gameObject.GetComponent<EnemigoEspadaController>().atacando == false)
                collision.gameObject.GetComponent<EnemigoEspadaVidaController>().PerderVida();
    }

    //Funci�n que controla si el enemigo se mantiene en el collider del ataque
    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.GetComponent<EnemigoBasicoVidaController>() != null) && (collision.gameObject.tag == "EnemigoDano"))
        {
            collision.gameObject.GetComponent<EnemigoBasicoVidaController>().PerderVida();
            //collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right, ForceMode2D.Impulse);
        }
        else if ((collision.gameObject.GetComponent<EnemigoCascoVidaController>() != null) && (collision.gameObject.tag == "EnemigoCasco"))
            collision.gameObject.GetComponent<EnemigoCascoVidaController>().PerderVida();
        else if ((collision.gameObject.GetComponent<EnemigoEspadaVidaController>() != null) && (collision.gameObject.tag == "EnemigoEspada"))
            if (collision.gameObject.GetComponent<EnemigoEspadaController>().atacando == false)
                collision.gameObject.GetComponent<EnemigoEspadaVidaController>().PerderVida();
    }

}
