﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private float velocidad = 1.0f;
    [SerializeField]
    private float fuerzaSalto = 2.5f;
    [SerializeField]
    private Transform comprobarSuelo;
    [SerializeField]
    private float comprobarRadioSuelo;
    [SerializeField]
    private LayerMask layerSuelo;


    private float inputHorizontal;
    private bool mirandoDerecha = true;
    private Rigidbody2D _rigidody;
    private Vector2 vectorMovimiento;
    private bool esSuelo = true;
    private Animator _animator;
    private bool caminar = true;

    private AudioSource _audio;
    public AudioClip clipSaltar;
    public AudioClip clipAtacar;


    //Se lanza antes de cualquier frame, al principio
    private void Awake()
    {
        //Inicializamos los componentes de jugador
        _rigidody = this.GetComponent<Rigidbody2D>();
        _animator = this.GetComponent<Animator>();
       // _renderer = this.GetComponent<Renderer>();
        _audio = this.GetComponent<AudioSource>();
    }

    //Nota:
    /*
	1）Update(): Este método se llama inmediatamente cada ejecución de trama. En él implementamos la lógica detecta cuando se quiere que se mueva o salta el jugador.
	2）LateUpdate(): LateUpdate se llama después de que se hayan llamado todos los métodos de Actualización. La usamos para las animaciones
	3）FixedUpdate(): Actualización fija. Por defecto, el sistema se llama cada 0.02 segundos, y el tiempo de intervalo específico. La usamos para las operaciones con física.
	*/
    private void Update()
    {
        //---------------------MOVERSE-----------------
        //Obtenemos la información del input horizontal (-1 izquierda, 1 derecha) y guardamos el vector de movimiento con la información del input. Indicamos también que la "y" es 0.
        //Se utiliza GetAxisRaw para que en cualquier momento devuelva 1 o -1 del input del teclado. Si se utiliza GetAxis, te devuelve el valor una vez el personaje haya acabado y suavizado el movimiento        
        inputHorizontal = Input.GetAxisRaw("Horizontal") * velocidad;
        vectorMovimiento = new Vector2(inputHorizontal, 0);

        //Si el personaje no camina, la velocidad será cero y no podrá girarse
        if (caminar == false)
            vectorMovimiento = new Vector2(0, 0);
        else { 
        //Si el movimiento no corresponde con hacia mira el personaje, lo giramos
        if (mirandoDerecha == true && inputHorizontal < 0)
            Flip();
        if (mirandoDerecha == false && inputHorizontal > 0)
            Flip();
        }

        //----------------------SALTAR....................
        //¿Está en el suelo? OverlapCircle va lanzando bolitas en la posición indicada y con el radio indicado y comprueba si colisionan con el layer establecido
        esSuelo = Physics2D.OverlapCircle(comprobarSuelo.position, comprobarRadioSuelo, layerSuelo);

        //Si pulsamos saltar y está en el suelo/plataforma, se llama a la función de saltar
        if ((Input.GetButtonDown("Jump") || Input.GetKeyDown("z")) && esSuelo)
            Saltar();
        else if (Input.GetKeyDown("x") || (Input.GetMouseButtonDown(0))) //Si se pulsa C o click del ratón, se ataca
            StartCoroutine("Atacar");
    }

   
    private void FixedUpdate()
    {
        //Obtenemos la velocidad horizontal y se la aplicamos al rigibody. No se pone la "y" a cero porque en caso de que el personaje cayera de una plataforma, nunca bajaría, se estaría forzando a no moverse.
        float velocidadHorizontal = vectorMovimiento.normalized.x * velocidad;
        _rigidody.velocity = new Vector2(velocidadHorizontal, _rigidody.velocity.y);
    }

    //Última función a ejecutar antes de pintar o ejecutar el frame. Se suele utilizar para el código del animator
    private void LateUpdate()
    {
        //El personaje estará en reposo siempre y cuando el vector movimiento sea cero
        //_animator.SetBool("Reposo", vectorMovimiento == Vector2.zero);
        _animator.SetBool("EsSuelo", esSuelo);
        _animator.SetInteger("Movimiento", (int)inputHorizontal);
    }

    //Función que gira al personaje si cambia de dirección
    private void Flip()
    {
        mirandoDerecha = !mirandoDerecha;
        float localScaleX = this.transform.localScale.x;
        localScaleX = localScaleX * -1f;
        this.transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }

    //Función que aplica la mecánica de salto del jugador
    private void Saltar()
    {
        if (esSuelo)
        {
            _audio.PlayOneShot(clipSaltar);
            //Se aplica una fuerza hacia arriba y se aplica de manera impulso
            _rigidody.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }
    }

    //Función que se llama al atacar
    private IEnumerator Atacar()
    {
        //Ataca si el juego no está pausado
        if (GameObject.FindGameObjectWithTag("UILobelia").GetComponent<UIButtonController>().juegoPausado == false) { 
            //Mientras se ataca no se puede mover el personaje.
            caminar = false;
            _audio.PlayOneShot(clipAtacar);
            _animator.SetBool("Ataque", true);     
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(true);
            yield return new WaitForSeconds(0.8f);
            this.GetComponent<Transform>().GetChild(2).gameObject.SetActive(false);
            _animator.SetBool("Ataque", false);
            caminar = true;
        }
    }

    public void InmobilizarPlayer()
    {
        velocidad = 0.0f;
        fuerzaSalto = 0.0f;
    }

}
