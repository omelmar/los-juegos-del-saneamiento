using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerAtaqueController : MonoBehaviour
{
    [SerializeField]
    private bool AtaqueConParaguas = false;

    private Rigidbody2D _rigibody;
    private float fuerzaImpulso = 1.0f;

    private void Awake()
    {
        _rigibody = this.GetComponent<Transform>().parent.GetComponent<Rigidbody2D>();
    }

    //Funci�n que controla si el ataque (salto o golpe de paraguas) colisiona con alg�n collider
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si el ataque no se hace con el paraguas, sino saltando encima
        if(AtaqueConParaguas == false) {            
            //Si salta encima de un enemigo con el tag "EnemigoDano", le hacemos da�o
            if (collision.gameObject.tag == "EnemigoDano")
            {
                //Si en el momento de chocar con el enemigo, Player no est� parpadeando porque le han hecho da�o, da�ar� al enemigo
                _rigibody.AddForce(Vector2.up * fuerzaImpulso, ForceMode2D.Impulse);
                collision.gameObject.GetComponent<EnemigoBasicoVidaController>().PerderVida();
            }
            else if (collision.gameObject.tag == "EnemigoEspada") {                
                _rigibody.AddForce(Vector2.up * fuerzaImpulso, ForceMode2D.Impulse);
                collision.gameObject.GetComponent<EnemigoEspadaVidaController>().PerderVida();
            }
        }
       /* else //Si se ataca con el paraguas
        {
            if (collision.gameObject.tag == "EnemigoDano")
            {                
                collision.gameObject.GetComponent<EnemigoBasicoVidaController>().PerderVida();
                //collision.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right, ForceMode2D.Impulse);
            }
            else if(collision.gameObject.tag == "EnemigoCasco")
                collision.gameObject.GetComponent<EnemigoCascoVidaController>().PerderVida();
            else if (collision.gameObject.tag == "EnemigoEspada")
                if(collision.gameObject.GetComponent<EnemigoEspadaController>().atacando == false)
                    collision.gameObject.GetComponent<EnemigoEspadaVidaController>().PerderVida();
        }*/
    }
}
