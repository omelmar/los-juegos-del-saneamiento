using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cucharas : MonoBehaviour
{
    [SerializeField]
    private bool cucharaMadera = false; //10 puntos
    [SerializeField]
    private bool cucharaPlata = false; //50 puntos
    [SerializeField]
    private bool cucharaOro = false; //125 puntos

    private int puntosObtenidos = 0;
    private GameObject gameplayManager;
    private AudioSource _audio;

    private void Awake()
    {
        gameplayManager = GameObject.FindGameObjectWithTag("GameControllerLobelia");
        _audio = this.GetComponent<AudioSource>();
    }

    //Si el player entra en el trigger, se llama a SumarPuntos()
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "PlayerLobelia")
        {
            StartCoroutine("SumarPuntos");
        }
    }

    //Funci�n que va a sumar los puntos en el Game Controller. los puntos dependen de la cuchara cogida
    private IEnumerator SumarPuntos()
    {
        if (cucharaMadera == true)
            puntosObtenidos = 10;
        else if (cucharaPlata == true)
            puntosObtenidos = 50;
        else if (cucharaOro == true)
            puntosObtenidos = 125;

        //Llamamos al Game Controller para que sume los puntos y se elimina el objeto
        gameplayManager.GetComponent<GameLobeliaController>().GanarPuntos(puntosObtenidos);
        _audio.Play();
        yield return new WaitForSeconds(0.2f);
        Destroy(this.gameObject);
    }
}
