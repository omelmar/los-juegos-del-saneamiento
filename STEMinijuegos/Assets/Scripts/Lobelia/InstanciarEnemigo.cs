using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarEnemigo : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemigoAInstanciar;
    [SerializeField]
    private float tiempoEntreInstancias = 10.0f;
    [SerializeField]
    private int numeroEnemigos = 1;
    [SerializeField]
    private GameObject playManager;

    /*[SerializeField]
    private int  maximoInstancias = 10;

    private int numInstancias = 0;*/

    private bool partidaConrollerComenzada = false;
    private bool inicioInstanciar = false;

    void Awake()
    {
        numeroEnemigos = 1;
        //playManager = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        //Si la partida no ha comenzado, vamos revisando la variable para ver cuando comienza
        if (partidaConrollerComenzada == false)
        {
            partidaConrollerComenzada = playManager.GetComponent<GameLobeliaController>().partidaComenzada;
            //Si el controller ya ha iniciado la partida, pero no hemos comenzado a instanciar, lo hacemos
            if((partidaConrollerComenzada == true) && (inicioInstanciar == false)){
                inicioInstanciar = true;
                StartCoroutine("InstanciaEnemigo");
            }
        }
        //Vamos continuamente comprobando el nivel del juego para ir acumentando la variedad de enemigos
        numeroEnemigos = playManager.GetComponent<GameLobeliaController>().nivel;
    }

    //Funci�n que va a ir instanciando enemigos en un intervalo de tiempo editable
    private IEnumerator InstanciaEnemigo()
    {
        //Comenzamos a instanciar enemigos cuando comienza la partida
        if ((partidaConrollerComenzada == true) && (inicioInstanciar == true)) {
            yield return new WaitForSeconds(tiempoEntreInstancias);
            //Al principio solo estar� diposnible el enemigo 0 de la lista, a medida que se ganan puntos, "numeroEmemigos" ir variando hasta llegar a cuatro.
            GameObject.Instantiate(enemigoAInstanciar[Random.Range(0, numeroEnemigos)], transform.position, Quaternion.identity);
            StartCoroutine("InstanciaEnemigo");
        }
    }
}
