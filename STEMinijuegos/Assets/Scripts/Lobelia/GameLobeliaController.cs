using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameLobeliaController : MonoBehaviour
{
    [SerializeField]
    private TMP_Text txtCucharas;
    [SerializeField]
    private TMP_Text txtPuntuacionFinal;
    [SerializeField]
    private UIButtonController buttonController;
    [SerializeField]
    private GameObject panelIntroPartida;
    [SerializeField]
    private GameObject panelFinPartida;
    [SerializeField]
    private AudioClip clipJuego;
    [SerializeField]
    private AudioClip clipFinPartida;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private GameObject player;

    private int puntosCucharas = 0;

    public int nivel = 1;
    public bool partidaComenzada = false;
  
    // Start is called before the first frame update
    void Awake()
    {
        Time.timeScale = 1.0f;
        txtCucharas.text = "0";
        nivel = 1;
        partidaComenzada = false;
    }

    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true)
                ComenzarPartida();
            else
                buttonController.Pausar();
        }
    }

    //Al principio se mostrar� una intro y cuando se sale, comenzar� la partida
    public void ComenzarPartida()
    {
        panelIntroPartida.SetActive(false);
        _audio.Stop();
        _audio.clip = clipJuego;
        _audio.Play();
        partidaComenzada = true;
        player.SetActive(true);        
    }
    //Funci�n que se llama cada vez que se ganan puntos (se coge cuchara)
    public void GanarPuntos(int txtPuntosCucharas)
    {
        //Se aumentan los puntos, se actualiza el marcador y se aumenta el nivel si corresponde
        puntosCucharas = puntosCucharas + txtPuntosCucharas;
        txtCucharas.text = puntosCucharas.ToString();
        //Al principio habr� un solo tipo de enemigo (nivel 1) y cada 500 puntos se aumetnar� un tipo de enemigo hasta llegar a cuatro niveles
        if ((nivel == 1) && (puntosCucharas > 500))
            nivel++;
        else if ((nivel == 2) && (puntosCucharas > 1000))
            nivel++;
        else if ((nivel == 3) && (puntosCucharas > 1500))
            nivel++;
    }

    public IEnumerator FinPartida()
    {
        _audio.Stop();
        _audio.PlayOneShot(clipFinPartida);
        yield return new WaitForSeconds(0.5f);
        player.SetActive(false);
        txtPuntuacionFinal.text = txtCucharas.text;
        panelFinPartida.SetActive(true);
    }
}
