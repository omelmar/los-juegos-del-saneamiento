using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBasicoAtaqueController : MonoBehaviour
{
    private int danoAtaque = 1;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si collider con el que colisionamos es el del player, le quitamos vida       
        if (collision.collider.tag == "PlayerLobelia")
        {
            collision.gameObject.GetComponent<PlayerVidaController>().PerderVidaPlayer(danoAtaque);
        }
    }
}
