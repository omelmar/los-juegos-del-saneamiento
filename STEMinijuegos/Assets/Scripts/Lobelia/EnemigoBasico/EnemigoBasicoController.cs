using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoBasicoController : MonoBehaviour
{
    [SerializeField]
    private float velocidad = 0.5f;
    [SerializeField]
    private bool mirandoDerecha = true;

    private Vector2 vectorMovimiento;
    private float direccionHorizontal;
    private Rigidbody2D _rigidbody;
    private float velocidadAux;
    private bool obstaculoDetectado = false;
    

    public bool parado = false;


    private void Awake()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }

    //Vamos indicando la direcci�n constantemente para crear el vector de movimiento
    private void Update()
    {
        //Si est� parado, el vector de movimiento ser� 0
        if (parado == true)
            vectorMovimiento = new Vector2(0, 0);
        else //Si est� en movimiento
        {
            if (mirandoDerecha == true)
            {
                direccionHorizontal = 1;
            }
            else
            {
                direccionHorizontal = -1;
            }
            vectorMovimiento = new Vector2(direccionHorizontal, 0);
        }
    }
    
    private void FixedUpdate()
    {
        if (velocidad > 0.0f)
        {
            //Obtenemos la velocidad horizontal y se la aplicamos al rigibody. No se pone la y a cero porque en caso de que el personaje cayera de una plataforma, nunca bajar�a, se estar�a forzando a no moverse.
            float velocidadHorizontal = vectorMovimiento.normalized.x * velocidad;
            _rigidbody.velocity = new Vector2(velocidadHorizontal, _rigidbody.velocity.y);
        }
    }
    //Funci�n que se lanza al colisionar con un collider

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(obstaculoDetectado == false)
            //Sea lo que sea con lo que colisionemos, el enemigo se gira al otro lado
            StartCoroutine("Girar");
    }

    //Funci�n para girar el personaje
    private IEnumerator Girar()
    {
        obstaculoDetectado = true;
        mirandoDerecha = !mirandoDerecha;
        float localScaleX = this.transform.localScale.x;
        localScaleX = localScaleX * -1f;
        this.transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
        //Esperamos 5 ms para seguir con las colisiones porque si se detectan demasiado r�pido, el enemigo se puede trabar
        yield return new WaitForSeconds(0.5f);
        obstaculoDetectado = false;
    }

    //Funci�n que se llama cuando se quiere que se pare el enemigo
    public void Parar()
    {
        velocidadAux = velocidad;
        velocidad = 0;
    }


    //Funci�n que se llama cuando se quiere que se recupera la marcha del enemigo
    public void Reanudar()
    {
        velocidad = velocidadAux;
    }
}
