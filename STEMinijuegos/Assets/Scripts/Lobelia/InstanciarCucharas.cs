using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarCucharas : MonoBehaviour
{
    [SerializeField]
    private GameObject[] cucharaAInstanciar;
    [SerializeField]
    private float tiempoEntreInstancias = 15.0f;
    [SerializeField]
    private int numeroCucharas = 2;
    [SerializeField]
    private int segundosHastaDestruccion = 10;

    private GameObject cucharaInstanciada;


    private void Awake()
    {
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("InstanciarEnemigoHobbitBasico");
    }

    // Update is called once per frame
    void Update()
    {

    }

    //Funci�n que va a ir instanciando enemigos en un intervalo de tiempo editable
    private IEnumerator InstanciarEnemigoHobbitBasico()
    {
        yield return new WaitForSeconds(tiempoEntreInstancias);
        //Al principio solo estar� diposnible el enemigo 0 de la lista, a medida que se ganan puntos, "numeroEmemigos" ir variando hasta llegar a cuatro.
        cucharaInstanciada = GameObject.Instantiate(cucharaAInstanciar[Random.Range(0, numeroCucharas)], transform.position, Quaternion.identity);
        //Al intanciar la cuchara pierde la rotaci�n que tiene el prefab, por lo que se aplica de nuevo
        Vector3 newRotation = new Vector3(0, 0, -36);
        cucharaInstanciada.transform.eulerAngles = newRotation;
        Destroy(cucharaInstanciada, segundosHastaDestruccion);
        StartCoroutine("InstanciarEnemigoHobbitBasico");
    }
}
