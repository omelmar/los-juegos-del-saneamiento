using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoCascoVidaController : MonoBehaviour
{
    [SerializeField]
    private int vida = 1;
    private Animator _animator;
    private Rigidbody2D _rigibody;
    //private float fuerzaDano = 0.8f;
    private bool invencible = false;
    private AudioSource _audio;
    private int puntosObtenidos = 50;
    private GameObject gameplayManager;

    public AudioClip clipDano;

    private void Awake()
    {
        _animator = this.GetComponent<Animator>();
        _rigibody = this.GetComponent<Rigidbody2D>();
        _audio = this.GetComponent<AudioSource>();
        gameplayManager = GameObject.FindGameObjectWithTag("GameControllerLobelia");
    }

    //Funci�n que se llama al ser da�ado y pierde una vida
    public void PerderVida()
    {
        if (invencible == false)
        {
            vida--;
            _audio.PlayOneShot(clipDano);
            //Si le queda m�s de una vida, se aplica una fuerza para el da�o
            if (vida > 0)
                //_rigibody.AddForce(Vector2.up * fuerzaDano, ForceMode2D.Impulse);
                StartCoroutine("Dano");
            else if (vida == 0) //Si se queda sin vidas, muere
                StartCoroutine("EnemigoMuere");
        }
    }

    //Funci�n que se llama si la vida del enemigo llega a cero
    private IEnumerator EnemigoMuere()
    {
        //Se obtiene la clase controller del padre y se para el movimiento del enemigo
        this.GetComponent<Transform>().GetComponent<EnemigoCascoController>().Parar();
        //Deshabilitamos los colider y congelamos las posiciones del enemigo.
        //Esto har� que lo podamos atravesar y no caiga al vac�o una vez muerto.
        _rigibody.constraints = RigidbodyConstraints2D.FreezeAll;
        this.GetComponent<BoxCollider2D>().enabled = false;
        
        //Ejecuamos la animaci�n de muerte y en dos segundos eliminamos el objeto
        _animator.SetBool("Muere", true);
        //Llamamos al Game Controller para que sume los puntos y se elimina el objeto
        gameplayManager.GetComponent<GameLobeliaController>().GanarPuntos(puntosObtenidos);
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }

    //Corrutina que se llama cuando se da�a al enemigo
    private IEnumerator Dano()
    {
        this.GetComponent<EnemigoCascoController>().parado = true;
        invencible = true;
        _animator.SetBool("Dano", true);
        yield return new WaitForSeconds(1.0f);
        this.GetComponent<EnemigoCascoController>().parado = false;
        invencible = false;
        _animator.SetBool("Dano", false);
    }
}
