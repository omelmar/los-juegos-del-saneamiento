﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaController : MonoBehaviour
{
    [SerializeField]
    private bool plataformaCae = true;
    [SerializeField]
    private bool desliza = false; 
    [SerializeField]
    private float velocidad = 0.5f;
    [SerializeField]
    private Transform objetivo;
    [SerializeField]
    private bool comienzaParada = false;
    private Vector3 inicio, fin;

    private Rigidbody2D _rigibody;
    private Transform _transform;

    // Start is called before the first frame update
    void Start()
    {
        _rigibody = this.gameObject.GetComponent<Rigidbody2D>();
        _transform = this.GetComponent<Transform>();

        //Eliminamos el objetivo como hijo para que no se mueva junto a la plataforma
        //Indicamos las posiciones de inicio y fin
        if (objetivo != null) { 
            objetivo.parent = null;
            inicio = _transform.position;
            fin = objetivo.position;
        }

        //Si la plataforma comienza parada, no se va a deslizar hasta que Redy se ponga encima
        if (comienzaParada)
            desliza = false;
    }


    // Update is called once per frame    
    private void FixedUpdate()
    {
        //Si es una plataforma deslizante, aplicamos el algoritmo de movimiento
        if (desliza == true) { 
            if(objetivo != null)
            {
                float fixedVel = velocidad * Time.deltaTime;
                //calcula la distancia entre el objeto y el objetivo y va moviendose hasta dicho objetivo
                _transform.position = Vector3.MoveTowards(transform.position, objetivo.position, fixedVel);
            }

            //Si llegamos a objetivo
            if(_transform.position == objetivo.position)
            {
                //Si la posición final es igual al principio, el objetivo pasará a la posición final, en caso contrario, pasará a la inicial
                objetivo.position = (objetivo.position == inicio) ? fin : inicio;
            }
        }

    }


    //Función que controla si el personaje ha colisionado (caido) en la plataforma
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Redy"){
            if (plataformaCae == true)            
                StartCoroutine("PlataformaComiezaACaer");
            //El transform del personaje pasa a ser hijo de la plataforma para que se mueva en conjunto con ella
            collision.transform.parent = _transform;
            if (comienzaParada)
                desliza = true;
        }
        else if (collision.gameObject.layer == 13) { 
            //Si cae un objeto, el transform del personaje pasa a ser hijo de la plataforma para que se mueva en conjunto con ella
            collision.transform.parent = _transform;
        }
    }

    //Función que se invoca cuando se sale del collider de la plataforma
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Redy")
        {
            //El transform del personaje deja de ser hijo de la plataforma
            collision.transform.parent = null;
        }
    }

    //Corrutina que hace que la plataforma caiga después de 0,3 segundos de haberla pisado
    private IEnumerator PlataformaComiezaACaer()
    {
        yield return new WaitForSeconds(0.3f);
        _rigibody.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
        yield return new WaitForSeconds(2.0f);
        Destroy(this.gameObject);
    }
  

}
