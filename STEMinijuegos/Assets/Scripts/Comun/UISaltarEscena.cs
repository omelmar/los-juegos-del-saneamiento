using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UISaltarEscena : MonoBehaviour
{

    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SaltarEscena();
        }
    }

    //Funci�n que salta la escena actual y llama a la siguiente
    public void SaltarEscena()
    {
        int escena = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(escena + 1, LoadSceneMode.Single);
    }
}
