using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonController : MonoBehaviour
{
    [SerializeField]
    private Sprite spriteSonido;
    [SerializeField]
    private Sprite spriteSinSonido;
    [SerializeField]
    private AudioSource audioMusica;
    [SerializeField]
    private GameObject panelOpciones;

    private AudioSource _audiosourceGenerico;

    [HideInInspector]
    public bool juegoPausado = false;
    [HideInInspector]
    public bool juegoSinSonido = false;

    //Variables para el boton Sonido
    [SerializeField]
    private Image imageSonido;


    private void Awake()
    {
        _audiosourceGenerico = this.GetComponent<AudioSource>();
    }

    //Funci�n que pausa e inicia el juego de nuevo
    public void Pausar()
    {
        //Al pausarlo, paramos el tiempo.
        //Desde el objeto de la pieza de puzzle se controla que si el juegoPausado == true, no mueva la ficha
        _audiosourceGenerico.Play();
        if (juegoPausado == false)
        {
            audioMusica.Stop();
            juegoPausado = true;
            panelOpciones.SetActive(true);
            Time.timeScale = 0.0f;
        }
        else
        {
            //Al reanudar la partida se ejecuta el sonido si no se ha silencado adrede
            if (juegoSinSonido == false)
                audioMusica.Play();
            juegoPausado = false;
            panelOpciones.SetActive(false);
            Time.timeScale = 1.0f;
        }
    }

    //Funci�n que activa o desactiva el sonido
    public void ActivarDesactivarSonido()
    {
        _audiosourceGenerico.Play();
        if (juegoSinSonido == false)
        {
            juegoSinSonido = true;
            imageSonido.sprite = spriteSinSonido;
            audioMusica.Stop();
        }
        else
        {
            juegoSinSonido = false;
            imageSonido.sprite = spriteSonido;
            audioMusica.Play();
        }
    }   
    
    //Funci�n que se llama para cargar de nuevo el nivel acutal
    public void ReiniciarNivel()
    {
        int escena = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }
}
