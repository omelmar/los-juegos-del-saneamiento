using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UICargarEscena : MonoBehaviour
{
    [SerializeField]
    private AudioSource _audioS;
    private GameObject comoJugar;
    private GameObject panelEscogerPersonaje;
    private GameObject panelEscogerNumeroJugadores;

    private void Awake()
    {
        if (GameObject.FindGameObjectWithTag("ComoJugar") != null)
        {
            comoJugar = GameObject.FindGameObjectWithTag("ComoJugar");
            comoJugar.SetActive(false);
        }

        if (GameObject.FindGameObjectWithTag("PanelElegirPersonaje") != null)
        {
            panelEscogerPersonaje = GameObject.FindGameObjectWithTag("PanelElegirPersonaje");
            panelEscogerPersonaje.SetActive(false);
        }

        if (GameObject.FindGameObjectWithTag("PanelElegirNumeroPersonajes") != null)
        {
            panelEscogerNumeroJugadores = GameObject.FindGameObjectWithTag("PanelElegirNumeroPersonajes");
            panelEscogerNumeroJugadores.SetActive(false);
        }

        //_audioS = GameObject.FindGameObjectWithTag("UI").GetComponent<AudioSource>();
    }

    //Funci�n que carga un nivel espec�fico
    public void CargarEscena(int escena)
    {
        _audioS.Play();
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }

    //Funci�n que se llama para cargar de nuevo el nivel acutal
    public void ReiniciarNivel()
    {
        int escena = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }

    //Funci�n que sale del juego
    public void SalirJuego()
    {
        Application.Quit();
    }


    public void MostrarOcultarPanelComoJugar()
    {
        if (comoJugar.activeSelf == true)
            comoJugar.SetActive(false);
        else
            comoJugar.SetActive(true);
    }
    //Esto se ejecutar� �nicamente en el nivel de La Comarca
    public void MostrarOcultarEscogerPersonaje()
    {
        if (panelEscogerPersonaje.activeSelf == true)
            panelEscogerPersonaje.SetActive(false);
        else
            panelEscogerPersonaje.SetActive(true);
    }

    //Esto se ejecutar� �nicamente en el nivel de Carrera
    public void MostrarOcultarEscogerNumeroJuagdores()
    {
        if (panelEscogerNumeroJugadores.activeSelf == true)
            panelEscogerNumeroJugadores.SetActive(false);
        else
            panelEscogerNumeroJugadores.SetActive(true);
    }
}
