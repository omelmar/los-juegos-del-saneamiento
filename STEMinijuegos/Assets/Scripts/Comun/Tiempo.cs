using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tiempo : MonoBehaviour
{
    [SerializeField]
    private int tiempoInicial = 60;
    
    public bool tiempoHaciaDelante = false;
    public bool cuentaAtras = false; //Variable que indica si el tiempo corre o no 
    [HideInInspector]
    public float segundosActuales;

    private TMP_Text _textTiempo;
    private bool tiempoDescuento = false;
    
    private AudioSource _audioSource;
 
    // Use this for initialization
    void Awake()
    {        
        _textTiempo = this.GetComponent<TMP_Text>();
       // _audioSource = this.GetComponent<AudioSource>();
    }

    private void Start()
    {
        cuentaAtras = true;      
        _textTiempo.text = tiempoInicial.ToString();
        segundosActuales = tiempoInicial;
    }
    // Update is called once per frame
    void Update()
    {
        //Si la opci�n es de cuenta atr�s
        if (cuentaAtras)
        {            
            //Si queda al menos un segundo, se decrementa el tiempo
            if (segundosActuales >= 1.0f)
            {                
                segundosActuales -= Time.deltaTime;
                _textTiempo.text = Mathf.Round(segundosActuales).ToString();
                //Si se llega a lo 10 �ltimos segundos, no se est� ya en descuento
                //Se para la m�sica de la c�mara (m�sica del juego) y comienza el sonido de cuenta atr�s
                if ((segundosActuales < 10.0f) && (!tiempoDescuento))
                {
                    tiempoDescuento = true;
                    //Paramos el sonido de la c�mara y ejecutamos el de la cuenta atr�s
                    //GameObject.Find("Main Camera").GetComponent<AudioSource>().Stop();
                    _audioSource.Play();
                }
            }
            else //Si se llega a 0 segundos
            {
                cuentaAtras = false;
                this.GetComponent<AudioSource>().Stop();
                //**** invocar gameover en el GameplayManager ****
                //GameObject.FindGameObjectWithTag("GameController").GetComponent<GameplayController>().PerderPartida();
            }
            
        }
        else if (tiempoHaciaDelante) //Si la opci�n es que tiempo corra hacia adelante
        {
            segundosActuales += Time.deltaTime;
            _textTiempo.text = Mathf.Round(segundosActuales).ToString();
        }
    }


    //Se reinicia el marcador, por ejemplo cuando se cambia de turno
    public void ReiniciarMarcador()
    {
        cuentaAtras = true;
        this.GetComponent<AudioSource>().Stop();
       // GameObject.Find("Main Camera").GetComponent<AudioSource>().Play();
        _textTiempo.text = tiempoInicial.ToString();
        segundosActuales = tiempoInicial;        
    }


    //Funci�n que se llama si en alg�n momento se consiguen segundos extras
    private void RegalarSegundos(float segundos)
    {
        _audioSource.Stop();
        segundos += segundos;
    }
}
