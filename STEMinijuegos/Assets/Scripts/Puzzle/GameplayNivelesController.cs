using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayNivelesController : MonoBehaviour
{
    [SerializeField]
    private AudioClip clipMenu;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private GameObject panelIntroPartida;

    private void Awake()
    {
        Time.timeScale = 1.0f;
    }

    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true) { 
                panelIntroPartida.SetActive(false);
                _audio.Stop();
                _audio.clip = clipMenu;
                _audio.Play();
            }
        }
    }
}
