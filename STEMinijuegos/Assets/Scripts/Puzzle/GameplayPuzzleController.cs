using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayPuzzleController : MonoBehaviour
{
    [HideInInspector]
    public bool puzzleTerminado = false;

    [SerializeField]
    private GameObject panelVictoria;
	[SerializeField]
    private GameObject panelTextoSam;
    [SerializeField]
    private GameObject txtTiempo;
    [SerializeField]
    private TMP_Text textTiempoPanelVictoria;
    [SerializeField]
    private GameObject panelTiempoOpciones;
    [SerializeField]
    private GameObject panelIlustracionCompleta;
    [SerializeField]
    private GameObject panelTablero;
    [SerializeField]
    private GameObject textCuentaAtras;
    [SerializeField]
    private AudioClip clipInicio;
    [SerializeField]
    private AudioClip clipMagia;

    private GameObject tablero;    
    private int numeroPuzzle = 0;
    private List<Sprite> listaSpritesPuzzle = new List<Sprite>();
    private bool comienzaCuentaAtras = false;
    private float seguntosCuentaAtras = 5.0f;

    private void Awake()
    {
        Time.timeScale = 1.0f;
        //Nada m�s cargar obtenemos el objeto "Tablero"
        tablero = GameObject.FindGameObjectWithTag("Tablero");
    }

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1.0f;
        //Inicializamos la lista local de pizas e indicamos que el puzzle no est� terminado
        InicializarListaPiezas();
        puzzleTerminado = false;
        //Comienza la cuenta atr�s e iniciamos el juego
        comienzaCuentaAtras = true;
        StartCoroutine("InicioJuego");

    }

    private void Update()
    {
        //Mientras dure la cuenta atr�s y no se haya llegado a cero, vamos descontando
        if ((comienzaCuentaAtras) && (seguntosCuentaAtras >= 0.0f))
        {
            seguntosCuentaAtras -= Time.deltaTime;
            textCuentaAtras.GetComponent<TMP_Text>().text = Mathf.Round(seguntosCuentaAtras).ToString();
        }
        else //Si se llega a cero no mostramos nada para que no aparecan n�mero negativos
            textCuentaAtras.GetComponent<TMP_Text>().text = "";
    }

    //Funci�n que inicializa la lista local de piezas
    private void InicializarListaPiezas()
    {
        /*
         * Se van a recorrer el tablero y se obtendr� cada uno de los hijos (N piezas de puzzle + 1 casilla vac�a).
         * En cada iteraci�n, se establecer� a la clase PiezaPuzzle el sprite que contiene la casilla y una posici�n
         * de la lista quenla principio ser� la del contador (0,1,2,3,...,N).
         * Finalmente se a�ade a la lista local de sprites, el sprite de la pieza actual.
         * */
        int contador = 0;
        foreach (Transform children in tablero.GetComponent<Transform>())
        {
            children.GetComponent<PiezaPuzzle>().spriteDelante = children.GetComponent<Image>().sprite;
            children.GetComponent<PiezaPuzzle>().posicionLista = contador;
            listaSpritesPuzzle.Add(children.GetComponent<Image>().sprite);            
            contador++;
        }

    }

    //Corrutina que hace que comience el juego
    private IEnumerator InicioJuego()
    {
        //Esperamos los 5 segundos de la cuenta atr�s y luego habiltiamos los paneles de juego, deshabilitamos la cuenta atr�s. 
        yield return new WaitForSeconds(5.0f);
        panelIlustracionCompleta.SetActive(false);
        panelTiempoOpciones.SetActive(true);
        textCuentaAtras.GetComponent<AudioSource>().Stop();
        textCuentaAtras.GetComponent<AudioSource>().PlayOneShot(clipInicio);
        yield return new WaitForSeconds(1.0f);
        this.GetComponent<AudioSource>().Play();
        textCuentaAtras.SetActive(false);
    }

    //Funci�n que actualiza la lista local de sprites
    public void ActualizarLista(int posPiezaAMover, int posPiezaVacia)
    {
        //Intercambiamos las posiciones de los sprites de la pieza a mover con la vac�a
        Sprite spriteTemporal = listaSpritesPuzzle[posPiezaAMover];
        listaSpritesPuzzle[posPiezaAMover] = listaSpritesPuzzle[posPiezaVacia];
        listaSpritesPuzzle[posPiezaVacia] = spriteTemporal;
        comienzaCuentaAtras = false;
    }

    //Funci�n que comprueba que el puzzle se ha finalizado
    public void ComprobarPuzzleTerminado()
    {
        numeroPuzzle = 0;
        puzzleTerminado = true;
        //Se recorre toda la lista local de sprites y si se encuentran todas en orden, se da el puzzle por finalizado.
        //Ejemplo de orden: SpritePuzzle_0, PritePuzzle_1, ... ,SpritePuzzle_N
        foreach (Sprite pieza in listaSpritesPuzzle)
        {
            if (pieza.name != "CasillaVacia") { 
                if (pieza.name != "SpritePuzzle_" + numeroPuzzle)
                {
                    puzzleTerminado = false;
                }
            }
            numeroPuzzle++;
        }


        if (puzzleTerminado)
            StartCoroutine("PuzzleCompletado");
    }

    //Corrutina que muestra el panel de puzzle completado
    private IEnumerator PuzzleCompletado()
     {
        this.GetComponent<AudioSource>().Stop();
        this.GetComponent<AudioSource>().PlayOneShot(clipMagia);
        txtTiempo.GetComponent<Tiempo>().tiempoHaciaDelante = false;
        float tiempoFinal = Mathf.Round(txtTiempo.GetComponent<Tiempo>().segundosActuales * 100.0f) * 0.01f;
        textTiempoPanelVictoria.text = tiempoFinal.ToString();
        panelTiempoOpciones.SetActive(false);
        panelTablero.SetActive(false);
        panelIlustracionCompleta.SetActive(true);
        yield return new WaitForSeconds(3.0f);        
        
        panelVictoria.SetActive(true);
		panelTextoSam.SetActive(true);
        
     }
    

    //Funci�n para el modo puzzle deslizante que muestra el puzzle completo
    public void MostrarPuzzleCompleto()
    {
        if(panelIlustracionCompleta.activeSelf)
            panelIlustracionCompleta.SetActive(false);
        else
            panelIlustracionCompleta.SetActive(true);
    }
}
