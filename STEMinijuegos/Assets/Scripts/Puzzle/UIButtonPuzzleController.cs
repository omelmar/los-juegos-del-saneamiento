using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIButtonPuzzleController : MonoBehaviour
{
    [SerializeField]
    private Sprite spriteSonido;
    [SerializeField]
    private Sprite spriteSinSonido;
    [SerializeField]
    private AudioSource audioMusica;

    private AudioSource _audiosourceGenerico;

    public bool juegoPausado = false;

    //Variables para el boton Sonido
    [SerializeField]
    private Image imageSonido;


    private void Awake()
    {
        _audiosourceGenerico = this.GetComponent<AudioSource>();
    }

    //Funci�n que pausa e inicia el juego de nuevo
    public void Pausar()
    {
        //Al pausarlo, paramos el tiempo.
        //Desde el objeto de la pieza de puzzle se controla que si el juegoPausado == true, no mueva la ficha
        _audiosourceGenerico.Play();
        if (juegoPausado == false)
        {
            audioMusica.Stop();
            juegoPausado = true;
            Time.timeScale = 0.0f;
        }
        else
        {
            audioMusica.Play();
            juegoPausado = false;
            Time.timeScale = 1.0f;
        }
    }

    //Funci�n que activa o desactiva el sonido
    public void ActivarDesactivarSonido()
    {
        _audiosourceGenerico.Play();
        if(imageSonido.sprite == spriteSonido)
        {
            imageSonido.sprite = spriteSinSonido;
            audioMusica.Stop();
        }
        else
        {
            imageSonido.sprite = spriteSonido;
            audioMusica.Play();
        }
    }

    public void Reintentar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

}
