using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PiezaPuzzle : MonoBehaviour
{
    [HideInInspector]
    public Sprite spriteDelante;
    [HideInInspector]
    public int posicionLista = 0;


    [SerializeField]
    private Transform posN;
    [SerializeField]
    private Transform posS;
    [SerializeField]
    private Transform posE;
    [SerializeField]
    private Transform posO;
    [SerializeField]
    private GameObject juegoPausadoObjet;
    [SerializeField]
    private GameObject gameplayPuzzleManager;
    [SerializeField]
    private GameObject piezaVacia;

    private AudioSource _audio;
    private bool detectadoN = false;
    private bool detectadoS = false;
    private bool detectadoE = false;
    private bool detectadoO = false;
    //private bool puedeMover = false;
  

    private void Awake()
    {
        //gameplayPuzzleManager = GameObject.FindGameObjectWithTag("GameControllerPuzzle");
        _audio = GetComponent<AudioSource>();
        //piezaVacia = GameObject.FindGameObjectWithTag("PiezaVacia");         
    }

    private void Start()
    {
        //Se deshabilita el grid para poder mover las piezas
        //Si no se hace, no se pueden mover, ya que es el grid quien controla la posici�n
        spriteDelante = this.GetComponent<Image>().sprite;
    }

    /*private void Update()
    {
        Debug.DrawLine(transform.position, posE.position, Color.red);
        Debug.DrawLine(transform.position, posN.position, Color.green);
        Debug.DrawLine(transform.position, posS.position, Color.yellow);
        Debug.DrawLine(transform.position, posO.position, Color.blue);
    }*/

    //Funci�n que se llama para mover una pieza
    public void MoverPieza()
    {
        //La pieza se mover� si el puzzle no ha sido terminado
        if((gameplayPuzzleManager.GetComponent<GameplayPuzzleController>().puzzleTerminado == false) && juegoPausadoObjet.GetComponent<UIButtonPuzzleController>().juegoPausado == false) {          
            //Primero lanzamos rayos en las cuatro direccines hasta los puntos cardinales de cada pieza.
            //Si se detecta la casilla vac�a, la pieza se puede mover.
            detectadoN = Physics2D.Linecast(this.transform.position, posN.position, 1 << LayerMask.NameToLayer("PiezaVacia"));      
            detectadoS = Physics2D.Linecast(this.transform.position, posS.position, 1 << LayerMask.NameToLayer("PiezaVacia"));    
            detectadoE = Physics2D.Linecast(this.transform.position, posE.position, 1 << LayerMask.NameToLayer("PiezaVacia")); 
            detectadoO = Physics2D.Linecast(this.transform.position, posO.position, 1 << LayerMask.NameToLayer("PiezaVacia"));
            if (detectadoN || detectadoS || detectadoE || detectadoO)
            {                                
                _audio.Play();
                //LLamamos al GaameplayController para que actualice la lista local de sprites.
                gameplayPuzzleManager.GetComponent<GameplayPuzzleController>().ActualizarLista(posicionLista, piezaVacia.GetComponent<PiezaPuzzle>().posicionLista);

                //Intercambiamos los sprites y actualizamos la posici�n
                Vector3 tempPosition = this.transform.position;
                this.transform.position = piezaVacia.transform.position;
                piezaVacia.transform.position = tempPosition;

                int posicionListaTemporal = posicionLista;
                posicionLista = piezaVacia.GetComponent<PiezaPuzzle>().posicionLista;
                piezaVacia.GetComponent<PiezaPuzzle>().posicionLista = posicionListaTemporal;

                Sprite spritePiezaTemporal = spriteDelante;
                spriteDelante = piezaVacia.GetComponent<PiezaPuzzle>().spriteDelante;
                piezaVacia.GetComponent<PiezaPuzzle>().spriteDelante = spritePiezaTemporal;
                //Comprobamos si el puzzle est� finalizado
                gameplayPuzzleManager.GetComponent<GameplayPuzzleController>().ComprobarPuzzleTerminado();
            }
        }
    }
	
}
