using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuCarreraCointroller : MonoBehaviour
{
    private GameObject NumeroJugadoresSeleccionado;
    private void Awake()
    {
        Time.timeScale = 1.0f;
        NumeroJugadoresSeleccionado = GameObject.FindGameObjectWithTag("ControllerElegirNumeroJugadores");
    }

    //Funci�n que carga el nivel de men� de juegos
    public void CargarMenuJuegos(int escena)
    {
        //En este nivel, usamos esta llamada en vez de la de UICarcarEscesa.cs porque necesitamos eliminar el objeto que creamos "NumeroJugadoresSeleccionado"
        //De esa manera, si se vuelve a jugar, se crea un nuevo objeto y no se mantiene este obsoleto ya que esto no se destruye y pasa al nivel de juego de la carrera
        Destroy(NumeroJugadoresSeleccionado);
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }
}
