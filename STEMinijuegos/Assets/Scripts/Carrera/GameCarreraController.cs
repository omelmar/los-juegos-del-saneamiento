using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameCarreraController : MonoBehaviour
{
    [SerializeField]
    private GameObject panelIntroPartida;
    [SerializeField]
    private GameObject panelFinCarrera;
    [SerializeField]
    private UIButtonController buttonController;
    [SerializeField]
    private AudioClip clipJuego;
    [SerializeField]
    private AudioClip clipVictoria;
    [SerializeField]
    private AudioClip clipCuentaAtras;
    [SerializeField]
    private AudioSource _audio;
    [SerializeField]
    private GameObject player1;
    [SerializeField]
    private GameObject player2;
    [SerializeField]
    private GameObject playerSolo;
    [SerializeField]
    private GameObject puntoMovilidadCamara;
    [SerializeField]
    private GameObject panelCuentaAtras;
    [SerializeField]
    private TMP_Text textoPlayerGanador;
    [SerializeField]
    private GameObject tiempo;
    [SerializeField]
    private TMP_Text txtTiempo;

    private int tiempoInicial = 3;
    private float segundosActuales;
    private bool cuentaAtras = false;
    private GameObject numeroJugadoresController;
    private bool modoUnJugador = true;

    private void Awake()
    {
        Time.timeScale = 1.0f;
        player1.SetActive(false);
        player2.SetActive(false);
        playerSolo.SetActive(false);
        segundosActuales = tiempoInicial;
        panelCuentaAtras.SetActive(false);
        tiempo.GetComponent<Tiempo>().tiempoHaciaDelante = false;
    }

    // Update is called once per frame
    void Update()
    {

        //Si queda al menos un segundo y hay cuenta atr�s, se decrementa el tiempo
        if ((segundosActuales >= 0.0f) && (cuentaAtras == true))
        {
            segundosActuales -= Time.deltaTime;
            panelCuentaAtras.transform.GetChild(0).GetComponent<TMP_Text>().text = Mathf.Round(segundosActuales).ToString();
        }
        else if ((segundosActuales < 0.0f) && (cuentaAtras == true))
        {
            cuentaAtras = false;
            panelCuentaAtras.transform.GetChild(0).GetComponent<TMP_Text>().text = "";
            ComenzarCarrera();
        }

        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true)
                ComenzarPartida();
            else
                buttonController.Pausar();
        }
        
        if(modoUnJugador == false) { 
            //La c�mara va a seguir a un objeto que se encuentra en medio de los dos player. Este objeto ser� hijo del player m�s adelantado
            //De esta forma, la c�mara se mover� con el player m�s adelantado
            if (player1.transform.position.y > player2.transform.position.y)
                puntoMovilidadCamara.transform.SetParent(player1.transform);
            else
                puntoMovilidadCamara.transform.SetParent(player2.transform);
        }
    }

    //Al principio se mostrar� una intro y cuando se sale, comenzar� la partida
    public void ComenzarPartida()
    {
        panelIntroPartida.SetActive(false);
        _audio.Stop();
        panelCuentaAtras.SetActive(true);
        _audio.PlayOneShot(clipCuentaAtras);
        cuentaAtras = true;
    }

   //Despu�s de la cuenta atr�s, comienza la partida
    public void ComenzarCarrera()
    {
        panelCuentaAtras.SetActive(false);  
        //_audio.Stop();
        _audio.clip = clipJuego;
        _audio.Play();
        tiempo.GetComponent<Tiempo>().tiempoHaciaDelante = true;

        //Se definen qu� jugadores van a participar (1 jugador o dos jugadores)
        numeroJugadoresController = GameObject.FindGameObjectWithTag("ControllerElegirNumeroJugadores");
        if (numeroJugadoresController.GetComponent<EscogerNumeroJugadores>().escogidoUnJugador == true)
        {
            player1.SetActive(false);
            player2.SetActive(false);
            playerSolo.SetActive(true);
            puntoMovilidadCamara.transform.SetParent(playerSolo.transform);
            modoUnJugador = true;
        }
        else
        {
            player1.SetActive(true);
            player2.SetActive(true);
            playerSolo.SetActive(false);
            modoUnJugador = false;
        }

    }
    
    public void FinCarrera(int playerGanador)
    {
        _audio.Stop();
        panelFinCarrera.SetActive(true);
        textoPlayerGanador.text = "Ganador Player " + playerGanador;
        txtTiempo.text = "Tiempo total: " + tiempo.GetComponent<TMP_Text>().text;
        _audio.PlayOneShot(clipVictoria);
        player1.SetActive(false);
        player2.SetActive(false);
        playerSolo.SetActive(false);
    }

    //Funci�n que carga el nivel de men�
    public void CargarMenuCarrera(int escena)
    {
        //En este nivel, usamos esta llamada en vez de la de UICarcarEscesa.cs porque necesitamos eliminar el objeto que viene del men� "HobbitPlayerSeleccionado"
        //De esa manera, si se vuelve a jugar, se crea un nuevo objeto y no se mantiene este obsoleto
        Destroy(numeroJugadoresController);
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }
}
