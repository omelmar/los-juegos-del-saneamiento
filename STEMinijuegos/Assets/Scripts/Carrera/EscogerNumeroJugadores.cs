using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscogerNumeroJugadores : MonoBehaviour
{
    public bool escogidoUnJugador = true;

    private int escenaCarrera = 10;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void EscogerUnJugador()
    {
        escogidoUnJugador = true;
        SceneManager.LoadScene(escenaCarrera, LoadSceneMode.Single);
    }

    public void EscogerDosJugadores()
    {
        escogidoUnJugador = false;
        SceneManager.LoadScene(escenaCarrera, LoadSceneMode.Single);
    }
}
