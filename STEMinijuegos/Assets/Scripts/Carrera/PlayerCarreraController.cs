using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCarreraController : MonoBehaviour
{
    [SerializeField]
    private float velocidad = 0.5f;
    [SerializeField]
    public bool esPlayer1 = true;

    private float inputVertical;
    private Vector2 vectorMovimientoV;
    private Animator _animator;
    private Rigidbody2D _rigidody;
    private Transform _transform;
    private int posAnimator = 0;
    private bool puedeMoverse = true;

    //Se lanza antes de cualquier frame, al principio
    private void Awake()
    {
        //Inicializamos los componentes de jugador
        _rigidody = this.GetComponent<Rigidbody2D>();
        _animator = this.GetComponent<Animator>();
        _transform = this.GetComponent<Transform>();
        posAnimator = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Detecamos si es el player1 o no para controlar si ha pulsado W o P para avanzar
        //W avanza player1
        //P avanza palyer2
        if ((esPlayer1 == false) && (Input.GetKeyDown(KeyCode.P)) && (puedeMoverse == true))
        {
            /*var velocityY = velocidad * 1;
            _rigidody.velocity = new Vector2(_rigidody.velocity.x, velocityY);*/
            _transform.position += Vector3.up * velocidad * Time.deltaTime;
            posAnimator = (posAnimator + 1) % 3;
        }

        if ((esPlayer1 == true) && (Input.GetKeyDown(KeyCode.W)) && (puedeMoverse == true))
        {
            _transform.position += Vector3.up * velocidad * Time.deltaTime;
            posAnimator = (posAnimator + 1) % 3;
        }
        
    }

    //�ltima funci�n a ejecutar antes de pintar o ejecutar el frame. Se suele utilizar para el c�digo del animator
    private void LateUpdate()
    {
        _animator.SetInteger("Pos", posAnimator);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Gallina")
            puedeMoverse = false;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Gallina")
            puedeMoverse = true;
    }

}