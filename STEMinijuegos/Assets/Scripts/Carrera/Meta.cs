using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meta : MonoBehaviour
{
    private int playerCruzaMeta = 1; //1 - player1, 2 - player2
    private GameObject gameController;

    private void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameControllerCarrera");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if((collision.tag == "PlayerCarrera1") || (collision.tag == "PlayerCarrera2"))
        {
            //Si cruza la meta el player1
            if (collision.GetComponent<PlayerCarreraController>().esPlayer1 == true)
                playerCruzaMeta = 1;
            else
                playerCruzaMeta = 2;
            gameController.GetComponent<GameCarreraController>().FinCarrera(playerCruzaMeta);
        }
    }
}
