using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PerrosController : MonoBehaviour
{
    [SerializeField]
    private Transform[] waypoints;
    [SerializeField]
    private float velocidad = 0.3f;

    private int wpActual = 0;
    private Transform _transform;
    private Transform objetivo;
    private GameObject gameplayManager;

    private void Awake()
    {
        _transform = this.gameObject.GetComponent<Transform>();
        //Al inicio,el objetivo ser� el primer WP
        objetivo = waypoints[wpActual].GetComponent<Transform>();
        gameplayManager = GameObject.FindGameObjectWithTag("GameControllerMaggot");
    }


    void FixedUpdate()
    {

        float fixedVel = velocidad * Time.deltaTime;
        //calcula la distancia entre el objeto y el objetivo y va moviendose hasta dicho objetivo
        _transform.position = Vector3.MoveTowards(transform.position, objetivo.position, fixedVel);

        if (_transform.position == objetivo.position)
        {
            wpActual = (wpActual + 1) % waypoints.Length;
            objetivo = waypoints[wpActual].GetComponent<Transform>();
        }

            //Variables para el animator y giro
            Vector2 dir = waypoints[wpActual].position - transform.position;
            GetComponent<Animator>().SetFloat("DirX", dir.x);
            GetComponent<Animator>().SetFloat("DirY", dir.y);
    }

    //Funci�n que se llama cuando el perro alzanza al player. En este caso es fin de partida.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerMaggot")
        {
            //El OnTriggerEnter2D se ejecuta por cada rigibody, al haber un trigger que controla el ladrido en un componente hijo,
            //debejos controlarlo para que no se ejecute el fin del juego con el trigger hijo mencionado
            if (this.transform.GetComponentInChildren<PerrosLadrido>().ladrando == false)
                gameplayManager.GetComponent<GameMaggotController>().FinPartida();
        }
    }

}
