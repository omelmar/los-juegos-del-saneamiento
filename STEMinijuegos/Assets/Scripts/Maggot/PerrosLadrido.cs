using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerrosLadrido : MonoBehaviour
{
    private AudioSource _audio;

    [HideInInspector]
    public bool ladrando = false;

    private void Awake()
    {
        _audio = this.GetComponent<AudioSource>();   
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerMaggot")
        {
            //El trigger enter se ejecuta por cada rigibody, al haber un trigger que controla el ladrido en un componente hijo,
            //debejos controlarlo para que no se ejecute el fin del juego con el trigger hijo mencionado
            ladrando = true;
            StartCoroutine("Ladrar");
        }
            
    }

    //Funci�n que reproduce el ladrido cuando se detecta cerca el player
    private IEnumerator Ladrar()
    {
        _audio.Play();
        yield return new WaitForSeconds(0.2f);
        _audio.Stop();
        ladrando = false;
    }
}
