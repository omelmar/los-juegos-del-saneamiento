using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameMaggotController : MonoBehaviour
{
    [SerializeField]
    private TMP_Text txtVerduras;
    [SerializeField]
    private TMP_Text txtPuntuacionFinalFinPartida;
    [SerializeField]
    private TMP_Text txtPuntuacionFinalSalida;
    [SerializeField]
    private GameObject panelTextoSalida;
    [SerializeField]
    private GameObject panelIntroPartida;
    [SerializeField]
    private UIButtonController buttonController;
    [SerializeField]
    private AudioClip clipJuego;
    [SerializeField]
    private AudioClip clipFinPartida;
    [SerializeField]
    private AudioClip clipSalidaLaberinto;    
    [SerializeField]
    private GameObject panelFinPartida;
    [SerializeField]
    private GameObject panelSalidaLaberinto;
    [SerializeField]
    private GameObject panelTiempo;
    [SerializeField]
    private AudioSource _audio;
    [HideInInspector]
    public bool tenemosSeta = false;

    private GameObject player;
    private int puntosTotalVerduras = 0;

    private void Awake()
    {
        Time.timeScale = 1.0f;
        txtVerduras.text = "0";
        tenemosSeta = false;
        panelTextoSalida.SetActive(false);
        player = GameObject.FindGameObjectWithTag("PlayerMaggot");
        player.SetActive(false);
        //Instanciamos la seta en uno de sus puntos
        int punto = Random.Range(0, 6);
        //Debug.Log("PuntoSeta_" + punto);
        GameObject.Find("PuntoSeta_" + punto).GetComponent<InstanciaSeta>().InstanciarSeta();
        panelTiempo.GetComponent<Tiempo>().tiempoHaciaDelante = false;
    }

    // Update is called once per frame
    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true)
                ComenzarPartida();
            else
                buttonController.Pausar();
        }
    }

    //Al principio se mostrar� una intro y cuando se sale, comenzar� la partida
    public void ComenzarPartida()
    {
        panelIntroPartida.SetActive(false);
        _audio.Stop();
        _audio.clip = clipJuego;
        _audio.Play();
        player.SetActive(true);
        panelTiempo.GetComponent<Tiempo>().tiempoHaciaDelante = true;
    }

    //Funci�n que se llama cada vez que se ganan puntos (se coge verdura)
    public void GanarPuntos(int txtPuntosCucharas)
    {
        //Se aumentan los puntos, se actualiza el marcador y se aumenta el nivel si corresponde
        puntosTotalVerduras = puntosTotalVerduras + txtPuntosCucharas;
        txtVerduras.text = puntosTotalVerduras.ToString();
    }

    //Funci�n que se llama al llegar a la salida del laberinto
    public void SalidaLaberito()
    {
        if (tenemosSeta == true) {
            _audio.Stop();
            txtPuntuacionFinalSalida.text = txtVerduras.text;
            panelSalidaLaberinto.SetActive(true);
            _audio.PlayOneShot(clipSalidaLaberinto);
            player.SetActive(false);
        }
        else
        {
            if (panelTextoSalida.activeSelf == true)
                panelTextoSalida.SetActive(false);
            else
                panelTextoSalida.SetActive(true);
        }
    }

    //Funci�n que se llama cuando alguno de los perros atrapa  al player
    public void FinPartida()
    {
        _audio.Stop();
        txtPuntuacionFinalFinPartida.text = txtVerduras.text;
        panelFinPartida.SetActive(true);
        _audio.PlayOneShot(clipFinPartida);
        player.SetActive(false);
    }
}
