using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Verduras : MonoBehaviour
{
    [SerializeField]
    private bool Lechuga = false; //10 puntos
    [SerializeField]
    private bool Zanahoria = false; //25 puntos
    [SerializeField]
    private bool Seta = false; //500 puntos

    private int puntosObtenidos = 0;
    private GameObject gameplayManager;
    private AudioSource _audio;
    private Image imgSeta;

    private void Awake()
    {
        gameplayManager = GameObject.FindGameObjectWithTag("GameControllerMaggot");
        _audio = this.GetComponent<AudioSource>();
        imgSeta = GameObject.FindGameObjectWithTag("spriteSeta").GetComponent<Image>();
        imgSeta.enabled = false;

    }

    //Si el player entra en el trigger, se llama a SumarPuntos()
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerMaggot")
        {
            StartCoroutine("SumarPuntos");
        }
    }

    //Funci�n que va a sumar los puntos en el Game Controller. los puntos dependen de la cuchara cogida
    private IEnumerator SumarPuntos()
    {
        if (Lechuga == true)
            puntosObtenidos = 10;
        else if (Zanahoria == true)
            puntosObtenidos = 25;
        else if (Seta == true) { 
            puntosObtenidos = 500;
            imgSeta.enabled = true;
            //LLama al GameController para indicar que tenemos la seta
            gameplayManager.GetComponent<GameMaggotController>().tenemosSeta = true;

        }

        _audio.Play();

        //Llamamos al Game Controller para que sume los puntos y se elimina el objeto
        gameplayManager.GetComponent<GameMaggotController>().GanarPuntos(puntosObtenidos);
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
    }
}
