﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHobbitController : MonoBehaviour
{
    [SerializeField]
    private float velocidad = 0.5f;

    private float inputHorizontal;
    private float inputVertical;
    private bool mirandoDerecha = true;
    private Vector2 vectorMovimientoH;
    private Vector2 vectorMovimientoV;
    private bool caminandoHorizontal = true;
    private Animator _animator;
    private Rigidbody2D _rigidody;

    //Se lanza antes de cualquier frame, al principio
    private void Awake()
    {
        //Inicializamos los componentes de jugador
        _rigidody = this.GetComponent<Rigidbody2D>();
        _animator = this.GetComponent<Animator>();
    }

    //Nota:
    /*
	1）Update(): Este método se llama inmediatamente cada ejecución de trama. En él implementamos la lógica detecta cuando se quiere que se mueva o salta el jugador.
	2）LateUpdate(): LateUpdate se llama después de que se hayan llamado todos los métodos de Actualización. La usamos para las animaciones
	3）FixedUpdate(): Actualización fija. Por defecto, el sistema se llama cada 0.02 segundos, y el tiempo de intervalo específico. La usamos para las operaciones con física.
	*/
    private void Update()
    {
        //---------------------MOVERSE-----------------
        //Obtenemos la información del input horizontal (-1 izquierda, 1 derecha) y guardamos el vector de movimiento con la información del input. Indicamos también que la "y" es 0.
        //Se utiliza GetAxisRaw para que en cualquier momento devuelva 1 o -1 del input del teclado. Si se utiliza GetAxis, te devuelve el valor una vez el personaje haya acabado y suavizado el movimiento        
        inputHorizontal = Input.GetAxisRaw("Horizontal") * velocidad;
        inputVertical = Input.GetAxisRaw("Vertical") * velocidad;
        vectorMovimientoH = new Vector2(inputHorizontal, 0);
        vectorMovimientoV = new Vector2(inputVertical, 0);

        /*//----Controlamos prioridades en el animator para que si ya se está caminando en un eje, si se pulsa a la vez el otro eje no cambie de animación
        if ((int)inputHorizontal != 0) { 
            caminandoHorizontal = true;
            _animator.SetBool("CaminandoH", caminandoHorizontal);
        }
        else
        {
            caminandoHorizontal = false;
            _animator.SetBool("CaminandoH", caminandoHorizontal);
        }
        //-----------------


        if((int)inputVertical !=0)
            _animator.SetBool("CaminandoV", true);
        else
            _animator.SetBool("CaminandoV", false);*/

        //Si el personaje no camina, la velocidad será cero y no podrá girarse
        if (caminandoHorizontal == false) { 
            vectorMovimientoH = new Vector2(0, 0);
        }
        else
        {
            //Si el movimiento no corresponde con hacia mira el personaje, lo giramos
            if (mirandoDerecha == true && inputHorizontal < 0)
                Flip();
            if (mirandoDerecha == false && inputHorizontal > 0)
                Flip();
        }        
    }

    private void FixedUpdate()
    {
        //Obtenemos la velocidad horizontal y se la aplicamos al rigibody. No se pone la "y" a cero porque en caso de que el personaje cayera de una plataforma, nunca bajaría, se estaría forzando a no moverse.
        float velocidadHorizontal = (vectorMovimientoH.normalized.x * velocidad)/2; //Dividimos entre dos porque así podemos poner una velocidad menor a uno
        float velocidadVertical = (vectorMovimientoV.normalized.x * velocidad)/2; //Dividimos entre dos porque así podemos poner una velocidad menor a uno

        _rigidody.velocity = new Vector2(velocidadHorizontal, velocidadVertical);
    }

    //Última función a ejecutar antes de pintar o ejecutar el frame. Se suele utilizar para el código del animator
    private void LateUpdate()
    {
        //El personaje estará en reposo siempre y cuando el vector movimiento sea cero
        _animator.SetInteger("MovimientoH", (int)inputHorizontal);
        _animator.SetFloat("MovimientoV", inputVertical);

        //----Controlamos prioridades en el animator para que si ya se está caminando en un eje, si se pulsa a la vez el otro eje no cambie de animación
        if ((int)inputHorizontal != 0)
        {
            caminandoHorizontal = true;
            _animator.SetBool("CaminandoH", caminandoHorizontal);
        }
        else
        {
            caminandoHorizontal = false;
            _animator.SetBool("CaminandoH", caminandoHorizontal);
        }
        //-----------------


        if (inputVertical > 0.1f) { 
            _animator.SetBool("CaminandoVArriba", true);
            _animator.SetBool("CaminandoVAbajo", false);
        }
        else if(inputVertical < -0.1f) {
            _animator.SetBool("CaminandoVArriba", false);
            _animator.SetBool("CaminandoVAbajo", true);
        }
        else
        {
            _animator.SetBool("CaminandoVArriba", false);
            _animator.SetBool("CaminandoVAbajo", false);
        }
    }

    //Función que gira al personaje si cambia de dirección
    private void Flip()
    {
        mirandoDerecha = !mirandoDerecha;
        float localScaleX = this.transform.localScale.x;
        localScaleX = localScaleX * -1f;
        this.transform.localScale = new Vector3(localScaleX, transform.localScale.y, transform.localScale.z);
    }
}
