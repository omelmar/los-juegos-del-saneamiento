using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salida : MonoBehaviour
{
    private GameObject gameplayManager;

    private void Awake()
    {
        gameplayManager = GameObject.FindGameObjectWithTag("GameControllerMaggot");

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerMaggot")
        {
            gameplayManager.GetComponent<GameMaggotController>().SalidaLaberito();
        }
    }

    //Si salimos del collider de salida porque no tenemos la seta, se vuelve a llamar a salidaLaberito para que oculte el mensaje
    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerMaggot") && (gameplayManager.GetComponent<GameMaggotController>().tenemosSeta == false))
        {
            gameplayManager.GetComponent<GameMaggotController>().SalidaLaberito();
        }
    }
}
