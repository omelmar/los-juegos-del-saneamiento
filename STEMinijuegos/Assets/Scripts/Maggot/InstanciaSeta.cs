using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciaSeta : MonoBehaviour
{
    [SerializeField]
    private GameObject setaInstanciar;

public void InstanciarSeta()
    {
        GameObject.Instantiate(setaInstanciar, this.transform.position, Quaternion.identity);
    }
}
