using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameMenuComarcaController : MonoBehaviour
{
    private GameObject HobbitPlayerSeleccionado;
    private void Awake()
    {
        Time.timeScale = 1.0f;
        HobbitPlayerSeleccionado = GameObject.FindGameObjectWithTag("ControllerElegirHobbitPlayer");
    }

    //Funci�n que carga el nivel de men� de juegos
    public void CargarMenuJuegos(int escena)
    {
        //En este nivel, usamos esta llamada en vez de la de UICarcarEscesa.cs porque necesitamos eliminar el objeto que creamos "HobbitPlayerSeleccionado"
        //De esa manera, si se vuelve a jugar, se crea un nuevo objeto y no se mantiene este obsoleto ya que esto no se destruye y pasa al nivel de jeugo de la Comarca
        Destroy(HobbitPlayerSeleccionado);
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }
}
