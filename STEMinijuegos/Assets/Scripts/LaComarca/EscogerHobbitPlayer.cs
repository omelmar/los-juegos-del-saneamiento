using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscogerHobbitPlayer : MonoBehaviour
{
    public bool escogidoHobbitFemenino = true;

    private int escenaLaComarca = 12;

    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    public void EscogerHobbitFemenino()
    {
        escogidoHobbitFemenino = true;
        SceneManager.LoadScene(escenaLaComarca, LoadSceneMode.Single);
    }

    public void EscogerHobbitMasculino()
    {
        escogidoHobbitFemenino = false;
        SceneManager.LoadScene(escenaLaComarca, LoadSceneMode.Single);
    }
}
