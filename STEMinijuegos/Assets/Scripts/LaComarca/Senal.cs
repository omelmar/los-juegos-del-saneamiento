using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Senal : MonoBehaviour
{
    [SerializeField]
    private GameObject panelTextSenal;
    [TextArea] //[TextArea] Attribute to make a string be edited with a height-flexible and scrollable text area.
    public string textoSenal = "";

    //Si el player entra en el trigger, se llama a SumarPuntos()
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerHobbitHComarca") || (collision.tag == "PlayerHobbitMComarca"))
        {
            //El componente Text (TMP) se ecnuentra en un hijo del panel
            panelTextSenal.transform.GetChild(0).GetComponent<TMP_Text>().text = textoSenal;
            panelTextSenal.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerHobbitHComarca") || (collision.tag == "PlayerHobbitMComarca"))
        {
            panelTextSenal.SetActive(false);
            panelTextSenal.transform.GetChild(0).GetComponent<TMP_Text>().text = "";
        }
    }
}
