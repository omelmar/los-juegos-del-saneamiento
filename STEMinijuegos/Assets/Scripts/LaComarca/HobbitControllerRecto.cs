using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HobbitControllerRecto : MonoBehaviour
{
    [SerializeField]
    private Transform waypoint;
    [SerializeField]
    private float velocidad = 0.3f;
    [SerializeField]
    private bool caminaDerecha = true;

    private Transform _transform;
    private Vector3 inicio, fin;
    private int direccion = 1;

    private void Awake()
    {
        _transform = this.gameObject.GetComponent<Transform>();

        if (waypoint != null)
        {
            waypoint.parent = null;
            inicio = _transform.position;
            fin = waypoint.position;
        }

        if (caminaDerecha)
            direccion = 1;
        else
            direccion = -1;
    }


    void FixedUpdate()
    {

        float fixedVel = velocidad * Time.deltaTime;
        //calcula la distancia entre el objeto y el objetivo y va moviendose hasta dicho objetivo
        _transform.position = Vector3.MoveTowards(transform.position, waypoint.position, fixedVel);

        if (_transform.position == waypoint.position)
        {
            //Si la posici�n final es igual al principio, el objetivo pasar� a la posici�n final, en caso contrario, pasar� a la inicial
            waypoint.position = (waypoint.position == inicio) ? fin : inicio;

            //Al llegar al final, el personaje se gira
            direccion *= -1;
            _transform.localScale = new Vector2(direccion, 1);
        }
    }


}
