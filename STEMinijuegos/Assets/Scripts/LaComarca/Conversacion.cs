using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Conversacion : MonoBehaviour
{
    [SerializeField]
    private GameObject panelTextSenal;
    [TextArea] //[TextArea] Attribute to make a string be edited with a height-flexible and scrollable text area.
    public string [] textoConversacion;

    private int numeroConversacion = 0;

    //Si el player entra en el trigger, se llama a SumarPuntos()
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerHobbitHComarca") || (collision.tag == "PlayerHobbitMComarca"))
        {
            //numeroConversacion = Random.Range(0, textoSenal.Length);
            numeroConversacion = (numeroConversacion + 1) % textoConversacion.Length;
            //El componente Text (TMP) se ecnuentra en un hijo del panel, el segundo hijo
            panelTextSenal.transform.GetChild(1).GetComponent<TMP_Text>().text = textoConversacion[numeroConversacion];
            panelTextSenal.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.tag == "PlayerHobbitHComarca") || (collision.tag == "PlayerHobbitMComarca"))
        {
            panelTextSenal.SetActive(false);
            panelTextSenal.transform.GetChild(1).GetComponent<TMP_Text>().text = "";
        }
    }
}
