using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerController : MonoBehaviour
{
    [SerializeField]
    private GameObject panelIntroPartida;
    [SerializeField]
    private UIButtonController buttonController;
    [SerializeField]
    private AudioClip clipJuego;    
    [SerializeField]
    private AudioSource _audio;

    //------ Selecci�n de Player-----
    [SerializeField]
    private GameObject playerHobbitFemenino;
    [SerializeField]
    private GameObject playerHobbitMasculino;
    [SerializeField]
    private CinemachineVirtualCamera cinemachinevc;

    private GameObject player;
    private GameObject HobbitPlayerSeleccionado;

    private void Awake()
    {
        Time.timeScale = 1.0f;
        HobbitPlayerSeleccionado = GameObject.FindGameObjectWithTag("ControllerElegirHobbitPlayer");
        if(HobbitPlayerSeleccionado.GetComponent<EscogerHobbitPlayer>().escogidoHobbitFemenino == true)
        {
            playerHobbitFemenino.SetActive(true);
            playerHobbitMasculino.SetActive(false);
            player = playerHobbitFemenino;
            cinemachinevc.Follow = playerHobbitFemenino.transform;
            cinemachinevc.LookAt = playerHobbitFemenino.transform;
        }
        else
        {
            playerHobbitMasculino.SetActive(true);
            playerHobbitFemenino.SetActive(false);
            player = playerHobbitMasculino;
            cinemachinevc.Follow = playerHobbitMasculino.transform;
            cinemachinevc.LookAt = playerHobbitMasculino.transform;
        }
    }

    private void Start()
    {     
        player.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true)
                ComenzarPartida();
            else
                buttonController.Pausar();
        }
    }

    //Al principio se mostrar� una intro y cuando se sale, comenzar� la partida
    public void ComenzarPartida()
    {
        panelIntroPartida.SetActive(false);
        _audio.Stop();
        _audio.clip = clipJuego;
        _audio.Play();
        player.SetActive(true);
    }

    //Funci�n que carga el nivel de men�
    public void CargarMenuComarca(int escena)
    {
        //En este nivel, usamos esta llamada en vez de la de UICarcarEscesa.cs porque necesitamos eliminar el objeto que viene del men� "HobbitPlayerSeleccionado"
        //De esa manera, si se vuelve a jugar, se crea un nuevo objeto y no se mantiene este obsoleto
        Destroy(HobbitPlayerSeleccionado);
        SceneManager.LoadScene(escena, LoadSceneMode.Single);
    }

}
