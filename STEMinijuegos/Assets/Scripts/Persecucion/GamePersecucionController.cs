using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GamePersecucionController : MonoBehaviour
{
    [SerializeField]
    private GameObject panelFinPartida;
    [SerializeField]
    private GameObject panelVictoria;
    [SerializeField]
    private GameObject panelIntroPartida;
    [SerializeField]
    private AudioSource audioS;
    [SerializeField]
    private UIButtonController buttonController;
    [SerializeField]
    private AudioClip clipFinPartida;
    [SerializeField]
    private AudioClip clipVictoria;
    [SerializeField]
    private AudioClip clipJuego;
    [SerializeField]
    private TMP_Text txtNuevoNivel;
    [SerializeField]
    private Transform[] waypointsPlayer;
    [SerializeField]
    private Transform[] waypointsRoca;
    [SerializeField]
    private GameObject tiempo;
    [SerializeField]
    private TMP_Text txtTiempoFinal;

    private GameObject player;
    private GameObject roca;
    private int nivelActual = 0;


    private void Awake()
    {
        Time.timeScale = 1.0f;
        player = GameObject.FindGameObjectWithTag("PlayerPersecucion");
        roca = GameObject.FindGameObjectWithTag("Roca");
        player.SetActive(false);
        roca.SetActive(false);
        tiempo.SetActive(false);
    }

    // Update is called once per frame
    private void Update()
    {
        //Si se pulsa escape, se pausa el juego
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (panelIntroPartida.activeSelf == true)
                ComenzarPartida();
            else
                buttonController.Pausar();
        }
    }

    //Al principio se mostrar� una intro y cuando se sale, comenzar� la partida
    public void ComenzarPartida()
    {
        panelIntroPartida.SetActive(false);
        audioS.Stop();
        audioS.clip = clipJuego;
        audioS.Play();
        player.SetActive(true);
        roca.SetActive(true);
        tiempo.SetActive(true);
    }

    //Funci�n que se llama al subir de nivel
    public void SubirNivel()
    {
        nivelActual++;       
        switch (nivelActual)
        {
            case 1:
                txtNuevoNivel.text = "�Un poquito m�s r�pido!";
                break;
            case 2:
                txtNuevoNivel.text = "�Vamos all�!";
                break;
            case 3:
                FinalExito();
                break;
        }            
    }

    //Funci�n que se llama cuando termina la partida
    public void FinPartida()
    {
        audioS.Stop();
        player.SetActive(false);
        roca.SetActive(false);
        audioS.PlayOneShot(clipFinPartida);
        panelFinPartida.SetActive(true);
    }

    //Funci�n que se llama al reintentar despu�s de finalizar la partida
    public void ReintentarSegunNivel()
    {
        player.transform.position = waypointsPlayer[nivelActual].transform.position;
        roca.transform.position = waypointsRoca[nivelActual].transform.position;
        audioS.Play();
        panelFinPartida.SetActive(false);
        player.SetActive(true);
        roca.SetActive(true);
    }


    //Funci�n que se llama cuando se finaliza con �xito el nivel
    public void FinalExito()
    {
        audioS.Stop();
        player.SetActive(false);
        roca.SetActive(false);
        txtTiempoFinal.text = tiempo.GetComponent<Tiempo>().segundosActuales.ToString();
        tiempo.SetActive(false);
        audioS.PlayOneShot(clipVictoria);
        panelVictoria.SetActive(true);
    }
}
