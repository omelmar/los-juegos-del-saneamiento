﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPersecucionController : MonoBehaviour
{

    [SerializeField]
    public float velocidad = 1.0f;
    [SerializeField]
    private float fuerzaSalto = 2.5f;
    [SerializeField]
    private Transform comprobarSuelo;
    [SerializeField]
    private float comprobarRadioSuelo;
    [SerializeField]
    private LayerMask layerSuelo;
    [SerializeField]
    private AudioClip clipSaltar;

    private float inputHorizontal;
    private Vector2 vectorMovimiento;
    private bool esSuelo = true;

    private Animator _animator;
    private Rigidbody2D _rigidody;
    private AudioSource _audio;



    //Se lanza antes de cualquier frame, al principio
    private void Awake()
    {
        //Inicializamos los componentes de jugador
        _rigidody = this.GetComponent<Rigidbody2D>();
        _animator = this.GetComponent<Animator>();
        // _renderer = this.GetComponent<Renderer>();
        _audio = this.GetComponent<AudioSource>();
    }

    //Nota:
    /*
	1）Update(): Este método se llama inmediatamente cada ejecución de trama. En él implementamos la lógica detecta cuando se quiere que se mueva o salta el jugador.
	2）LateUpdate(): LateUpdate se llama después de que se hayan llamado todos los métodos de Actualización. La usamos para las animaciones
	3）FixedUpdate(): Actualización fija. Por defecto, el sistema se llama cada 0.02 segundos, y el tiempo de intervalo específico. La usamos para las operaciones con física.
	*/
    private void Update()
    {
        //Siempre camina a la derecha, nunca para
        inputHorizontal = 1;
        vectorMovimiento = new Vector2(inputHorizontal, 0);

        //----------------------SALTAR....................
        //¿Está en el suelo? OverlapCircle va lanzando bolitas en la posición indicada y con el radio indicado y comprueba si colisionan con el layer establecido
        esSuelo = Physics2D.OverlapCircle(comprobarSuelo.position, comprobarRadioSuelo, layerSuelo);

        //Si pulsamos saltar y está en el suelo/plataforma, se llama a la función de saltar
        if ((Input.GetButtonDown("Jump") || (Input.GetMouseButtonDown(0)) && esSuelo))
            Saltar();  
    }


    private void FixedUpdate()
    {
        //Obtenemos la velocidad horizontal y se la aplicamos al rigibody. No se pone la "y" a cero porque en caso de que el personaje cayera de una plataforma, nunca bajaría, se estaría forzando a no moverse.
        float velocidadHorizontal = vectorMovimiento.normalized.x * velocidad;
        _rigidody.velocity = new Vector2(velocidadHorizontal, _rigidody.velocity.y);
    }

    //Última función a ejecutar antes de pintar o ejecutar el frame. Se suele utilizar para el código del animator
    private void LateUpdate()
    {
        //El personaje estará en reposo siempre y cuando el vector movimiento sea cero
        //_animator.SetBool("Reposo", vectorMovimiento == Vector2.zero);
        _animator.SetBool("EsSuelo", esSuelo);
        _animator.SetFloat("Movimiento", inputHorizontal);
    }

    //Función que aplica la mecánica de salto del jugador
    private void Saltar()
    {
        if (esSuelo)
        {
            _audio.PlayOneShot(clipSaltar);
            //Se aplica una fuerza hacia arriba y se aplica de manera impulso
            _rigidody.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }
    }   

}
