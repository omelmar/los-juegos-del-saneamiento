using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubirNivel : MonoBehaviour
{
    [SerializeField]
    private GameObject panelMensaje;

    private GameObject player;
    private GameObject roca;
    private GameObject gameController;

   

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("PlayerPersecucion");
        roca = GameObject.FindGameObjectWithTag("Roca");
        gameController = GameObject.FindGameObjectWithTag("GameControllerPersecucion");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerPersecucion")
            AumentarNivel();
    }

    //Funci�n que se llama al aumentar de nivel
    private void AumentarNivel()
    {
        player.GetComponent<PlayerPersecucionController>().velocidad += 0.5f;
        roca.GetComponent<RocaController>().velocidad += 0.5f;
        gameController.GetComponent<GamePersecucionController>().SubirNivel();
        StartCoroutine("MostrarMensaje");
    }

    //Funci�n que muestra el mensaje de que se ha aumentado de nivel
    private IEnumerator MostrarMensaje()
    {
        panelMensaje.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        panelMensaje.SetActive(false);
    }
}
