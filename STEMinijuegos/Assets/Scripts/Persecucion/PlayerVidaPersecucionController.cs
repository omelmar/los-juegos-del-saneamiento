using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVidaPersecucionController : MonoBehaviour
{
    private GameObject gameController;

    // Start is called before the first frame update
    void Awake()
    {
        gameController = GameObject.FindGameObjectWithTag("GameControllerPersecucion");
    }


    //Funci�n que se llama al recibir da�o el player
    public void PerderVidaPlayer()
    {
        gameController.GetComponent<GamePersecucionController>().StartCoroutine("FinPartida");
    }
}
