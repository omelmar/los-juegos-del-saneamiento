﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocaController : MonoBehaviour
{
    [SerializeField]
    public float velocidad = 0.5f;    

    private float inputHorizontal;
    private Vector2 vectorMovimiento;
    private Rigidbody2D _rigidody;
    private GameObject player;


    //Se lanza antes de cualquier frame, al principio
    private void Awake()
    {
        //Inicializamos los componentes de jugador
        _rigidody = this.GetComponent<Rigidbody2D>();
        player = GameObject.FindGameObjectWithTag("PlayerPersecucion");
    }

    //Nota:
    /*
	1）Update(): Este método se llama inmediatamente cada ejecución de trama. En él implementamos la lógica detecta cuando se quiere que se mueva o salta el jugador.
	2）LateUpdate(): LateUpdate se llama después de que se hayan llamado todos los métodos de Actualización. La usamos para las animaciones
	3）FixedUpdate(): Actualización fija. Por defecto, el sistema se llama cada 0.02 segundos, y el tiempo de intervalo específico. La usamos para las operaciones con física.
	*/
    private void Update()
    {
        //Siempre camina a la derecha, nunca para
        inputHorizontal = 1;
        vectorMovimiento = new Vector2(inputHorizontal, 0);
    }


    private void FixedUpdate()
    {
        //Obtenemos la velocidad horizontal y se la aplicamos al rigibody. No se pone la "y" a cero porque en caso de que el personaje cayera de una plataforma, nunca bajaría, se estaría forzando a no moverse.
        float velocidadHorizontal = vectorMovimiento.normalized.x * velocidad;
        _rigidody.velocity = new Vector2(velocidadHorizontal, _rigidody.velocity.y);
    }

    //Función que controla si algún objeto entra en el trigger de la roca
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerPersecucion")
            player.GetComponent<PlayerVidaPersecucionController>().PerderVidaPlayer();
    }

}
