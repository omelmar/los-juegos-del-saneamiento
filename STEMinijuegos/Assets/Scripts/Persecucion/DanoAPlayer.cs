using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanoAPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject player;

    private void Awake()
    {
        //player = GameObject.FindGameObjectWithTag("PlayerPersecucion");
    }


    //Si entra el player en el trigger, se llama a perder vida.
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "PlayerPersecucion")
        {
            player.GetComponent<PlayerVidaPersecucionController>().PerderVidaPlayer();
        }
    }

    //Si entra el player en el collider, se llama a perder vida.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "PlayerPersecucion")
        {
            player.GetComponent<PlayerVidaPersecucionController>().PerderVidaPlayer();
        }
    }
}
