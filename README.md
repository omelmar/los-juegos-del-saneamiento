# Los Juegos del Saneamiento

¡Fiesta en La Comarca!

## DESCRIPCIÓN
Los Juegos del Saneamiento es un juego de minijuegos 2D en el varios vecinos medianos competirán y se divertirán en esta nueva fiesta de La Comarca.

### Particularidades
- Existen cinco minijuegos diferentes, cada uno con sus particularidades y controles específicos.
- En el menú de cada minijuego hay una opción "Cómo Jugar" que especifica de qué trata el minijuego y cuáles son su controles.
- Hay un nivel extra de exploración que no es competitivo.

## DESARROLLO
El juego ha sido desarrollado con Unity 2021.3.8f1 y el Visual Studio integrado en el mismo.
El pixel art ha sido diseñado con Pyxel Edit.

## LICENCIAS Y RECURSOS
Los assets utilizados en el juego o son de creación propia o cedidos libremente, ya sea directamente o a través de https://www.freesound.org en el caso de la música.
El juego no tiene ningún uso comercial y ha sido creado como homenaje a la obra de J.R.R. Tolkien, por lo que no existe ningún tipo de ingreso por él.
El proyecto es totalmente público y se puede usar y modificar libremente, excepto para usos comerciales.

## PORTABLES
En la ruta Portables se encuetran los .zip de por cada plataforma. Se debe acceder a lal zip y darle al icono de la flecha de "Download".
Indicar que únicamente se ha probado en Windowsx64, desconozco su funciona en el resto de plataformas.
